# Loading screen package

[![Loading screen package](http://img.youtube.com/vi/waR0Fu4SUFM/0.jpg)](https://www.youtube.com/watch?v=waR0Fu4SUFM "Loading screen package")

This asset contains 12 different loading screens. They were tested on mobile platforms (Android, iOS). These can be used on any platform, possibly you will have to tweak a little bit in the animations. Most of them is white color based, so you can edit it’s color in the editor to easily to fit your project. All of them scales with the screen size, but there are some which are more suitable for wider screens (landscape type) or longer screens (portrait type). <br><br>

&#10071;<strong>  They are NOT async loading screens! &#10071; </strong><br> <br>

Here is a list of the suitable screen types for all loading screen on mobile device:
 - Loading screen 1: Landscape type <br>
- Loading screen 2: Landscape + portrait type <br>
- Loading screen 3: Landscape + portrait type <br>
- Loading screen 4: Landscape + portrait  type <br>
- Loading screen 5: Portrait type <br>
- Loading screen 6: Landscape + portrait type <br>
- Loading screen 7: Landscape type <br>
- Loading screen 8: Landscape + portrait type <br>
- Loading screen 9: Landscape + portrait type <br>
- Loading screen 10: Landscape + portrait  type <br>
- Loading screen 11: Landscape + portrait type <br>
- Loading screen 12: Landscape type <br><br>

<strong>Package contains:</strong><br>
- 12 prefabs <br>
- 12 demo scenes <br>
- all sprites for the loading screens (300x...x3000 pixels) <br>
- LoadingScreenManager script<br><br>

<strong>Unity Asset Store</strong><br>
https://assetstore.unity.com/packages/tools/gui/loading-screen-package-185611

<strong>Youtube preview</strong><br>
https://www.youtube.com/watch?v=waR0Fu4SUFM

If you have any questions, suggestions or feedback, please feel free to leave a review or contact me at <a href="mailto:petrahugyecz@gmail.com"> petrahugyecz@gmail.com </a> &#9996;

